﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Docitt.Applicant.Mocks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using Xunit;
using LendFoundry.EventHub.Client;
using Moq;
using System.Threading.Tasks;

namespace Docitt.Applicant.Tests
{
    public class ApplicantServiceTests
    {
        private Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();

        [Fact]
        public void ApplicantService_Get_WithEmptyOrNull_ThrowsArgumentException()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                }
            };
            Assert.Throws<ArgumentException>(() => GetApplicantService(dummyApplicants).Get(string.Empty));
            Assert.Throws<ArgumentException>(() => GetApplicantService(dummyApplicants).Get(" "));
            Assert.Throws<ArgumentException>(() => GetApplicantService(dummyApplicants).Get(null));
        }


        [Fact]
        public void ApplicantService_Get_NonExistingRiskCode_ThrowsNotFoundException()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                }
            };

            Assert.Throws<NotFoundException>(() => GetApplicantService(dummyApplicants).Get("nonexisting"));
        }

        [Fact]
        public void ApplicantService_Get_ExistingRecord_ReturnsRecord()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                }
            };
            var result = GetApplicantService(dummyApplicants).Get("123");
            Assert.NotNull(result);
            Assert.Equal("Nayan",result.FirstName);
            Assert.Equal("Paregi", result.LastName);
        }


        [Fact(Skip = "Now email duplicate check removed")]
        public void ApplicantService_Add_Record_ThrowsUserAlreadyExist()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                }
            };
            Assert.Throws<ApplicantWithSameEmailAlreadyExist>(() => GetApplicantService(dummyApplicants).Add(new ApplicantRequest
            {
                Email = "nayan.paregi@gmail.com",
            }));
        }

        [Fact]
        public  void ApplicantService_Add_NullOrInvalidValues_ThrowsException()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                }
            };
            var service = GetApplicantService(dummyApplicants);
            Assert.Throws<ArgumentNullException>(() => service.Add(null));
            Assert.Throws<ValidationException>(() => service.Add(new ApplicantRequest
            {
                FirstName = "",
                LastName = "",
                Email = ""
            }));
        }

        [Fact]
        public void ApplicantService_Search_ReturnsRecord()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com",
                    Ssn = "123"
                }
            };

            var results =
                GetApplicantService(dummyApplicants).Search(new SearchApplicantRequest
                {
                    Ssn = "123",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com"
                });

            Assert.NotNull(results);
            var applicants = results as IApplicant[] ?? results.ToArray();
            Assert.Equal("Nayan", applicants.First().FirstName);
            Assert.Equal("Paregi", applicants.First().LastName);
        }

        [Fact]
        public void ApplicantService_Search_ReturnsRecord1()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com",
                    Ssn = "123"
                }
            };
            var results =
                GetApplicantService(dummyApplicants).Search(new SearchApplicantRequest
                {
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "Invalid@invalid.com"
                });

            Assert.NotNull(results);
            var applicants = results as IApplicant[] ?? results.ToArray();
            Assert.Equal(1, applicants.Length);
            Assert.Equal("Invalid", applicants.First().FirstName);
            Assert.Equal("Invalid", applicants.First().LastName);
        }

        [Fact]
        public void ApplicantService_Search_ReturnsRecord2()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com",
                    Ssn = "123"
                }
            };
            var results =
                GetApplicantService(dummyApplicants).Search(new SearchApplicantRequest
                {
                    FirstName = "Nayan",
                    LastName = "Paregi"
                });

            Assert.NotNull(results);
            var applicants = results as IApplicant[] ?? results.ToArray();
            Assert.Equal(1,applicants.Length);
            Assert.Equal("Nayan", applicants.First().FirstName);
            Assert.Equal("Paregi", applicants.First().LastName);
        }

        [Fact]
        public void ApplicantService_Delete_Record_ThrowsNotFoundException()
        {
            Assert.Throws<NotFoundException>(() => GetApplicantService().Delete("NOTEXIST"));
        }

        [Fact]
        public void ApplicantService_Delete_NullOrInvalidValues_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => GetApplicantService().Delete(null));
            Assert.Throws<ArgumentException>(() => GetApplicantService().Delete(""));
        }

        [Fact]
        public void ApplicantService_Delete_Success()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com",
                    Ssn = "123"
                }
            };
            GetApplicantService(dummyApplicants).Delete("123");
        }

        [Fact]
        public void ApplicantService_UpdateName_Record_ThrowsApplicantIdNullOrEmpty()
        {
            var dummyApplicantName = new ApplicantNameRequest
            {
                    FirstName = "Kinjal",
                    MiddleName = "N",
                    LastName = "Shah",
                    GenerationOrSuffix = "Mr."                
            };
            Assert.Throws<ArgumentException>(() => GetApplicantService().UpdateName(null, dummyApplicantName, "applicationNumber001"));
        }

        [Fact]
        public void ApplicantService_UpdateName_Record_ThrowsApplicantIdInvalid()
        {
            var dummyApplicantName = new ApplicantNameRequest
            {
                FirstName = "Kinjal",
                MiddleName = "N",
                LastName = "Shah",
                GenerationOrSuffix = "Mr."
            };
            Assert.Throws<NotFoundException>(() => GetApplicantService().UpdateName("invalid", dummyApplicantName, "applicationNumber001"));
        }
        [Fact]
        public void ApplicantService_UpdateName_Record_ThrowsApplicantNameRequestNull()
        {
            
            Assert.Throws<ArgumentNullException>(() => GetApplicantService().UpdateName("123", null, "applicationNumber001"));
        }
        [Fact]
        public void ApplicantService_UpdateName_Record_ThrowsApplicantNameRequestFirstNameNull()
        {
            var dummyApplicantName = new ApplicantNameRequest
            {
                FirstName = "",
                MiddleName = "",
                LastName = "Paregi",
                GenerationOrSuffix = ""
            };
            Assert.Throws<ArgumentException>(() => GetApplicantService().UpdateName("123", dummyApplicantName, "applicationNumber001"));
        }
        [Fact]
        public void ApplicantService_UpdateName_Record_ThrowsApplicantNameRequestLastNameNull()
        {
            var dummyApplicantName = new ApplicantNameRequest
            {
                FirstName = "Nayan",
                MiddleName = "",
                LastName = "",
                GenerationOrSuffix = ""
            };
            Assert.Throws<ArgumentException>(() => GetApplicantService().UpdateName("123", dummyApplicantName, "applicationNumber001"));
        }
        [Fact]
        public void ApplicantService_UpdateName_FirstNameOnly_ApplicantNameRequest()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com",
                    Ssn = "123"
                }
            };

            var dummyApplicantName = new ApplicantNameRequest
            {
                FirstName = "Kinjal",
                MiddleName = "",
                LastName = "Paregi",
                GenerationOrSuffix = ""
            };
            GetApplicantService(dummyApplicants).UpdateName("123", dummyApplicantName, "applicationNumber001");

            var result = GetApplicantService(dummyApplicants).Get("123");
            Assert.NotNull(result);
            Assert.Equal("Kinjal", result.FirstName);
            Assert.Equal("", result.MiddleName);
            Assert.Equal("Paregi", result.LastName);
            Assert.Equal("", result.GenerationOrSuffix);
        }

        [Fact]
        public void ApplicantService_UpdateName_MiddleNameOnly_ApplicantNameRequest()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Kinjal",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com",
                    Ssn = "123"
                }
            };

            var dummyApplicantName = new ApplicantNameRequest
            {
                FirstName = "Kinjal",
                MiddleName = "N",
                LastName = "Paregi",
                GenerationOrSuffix = ""
            };
            GetApplicantService(dummyApplicants).UpdateName("123", dummyApplicantName, "applicationNumber001");

            var result = GetApplicantService(dummyApplicants).Get("123");
            Assert.NotNull(result);
            Assert.Equal("Kinjal", result.FirstName);
            Assert.Equal("N", result.MiddleName);
            Assert.Equal("Paregi", result.LastName);
            Assert.Equal("", result.GenerationOrSuffix);
        }

        [Fact]
        public void ApplicantService_UpdateName_LastNameOnly_ApplicantNameRequest()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Kinjal",
                    MiddleName = "N",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com",
                    Ssn = "123"
                }
            };

            var dummyApplicantName = new ApplicantNameRequest
            {
                FirstName = "Kinjal",
                MiddleName = "N",
                LastName = "Shah",
                GenerationOrSuffix = ""
            };
            GetApplicantService(dummyApplicants).UpdateName("123", dummyApplicantName, "applicationNumber001");

            var result = GetApplicantService(dummyApplicants).Get("123");
            Assert.NotNull(result);
            Assert.Equal("Kinjal", result.FirstName);
            Assert.Equal("N", result.MiddleName);
            Assert.Equal("Shah", result.LastName);
            Assert.Equal("", result.GenerationOrSuffix);
        }

        [Fact]
        public void ApplicantService_UpdateName_GenerationOrSuffixOnly_ApplicantNameRequest()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Kinjal",
                    MiddleName = "N",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com",
                    Ssn = "123"
                }
            };

            var dummyApplicantName = new ApplicantNameRequest
            {
                FirstName = "Kinjal",
                MiddleName = "N",
                LastName = "Shah",
                GenerationOrSuffix = "Mr."
            };
            GetApplicantService(dummyApplicants).UpdateName("123", dummyApplicantName, "applicationNumber001");

            var result = GetApplicantService(dummyApplicants).Get("123");
            Assert.NotNull(result);
            Assert.Equal("Kinjal", result.FirstName);
            Assert.Equal("N", result.MiddleName);
            Assert.Equal("Shah", result.LastName);
            Assert.Equal("Mr.", result.GenerationOrSuffix);
        }

        [Fact]
        public void ApplicantService_UpdateName_Record_ApplicantNameRequest()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    Email = "nayan.paregi@gmail.com",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    FirstName = "Invalid",
                    LastName = "Invalid",
                    Email = "Invalid@invalid.com",
                    Ssn = "123"
                }
            };

            var dummyApplicantName = new ApplicantNameRequest
            {
                FirstName = "Kinjal",
                MiddleName = "N",
                LastName = "Shah",
                GenerationOrSuffix = "Mr."
            };

            EventHubClient.Setup(e => e.Publish(It.IsAny<string>(), It.IsAny<object>())).Returns(Task.FromResult(true));

            GetApplicantService(dummyApplicants).UpdateName("123", dummyApplicantName, "applicationNumber001");

            var result = GetApplicantService(dummyApplicants).Get("123");
            Assert.NotNull(result);
            Assert.Equal("Kinjal", result.FirstName);
            Assert.Equal("N", result.MiddleName);
            Assert.Equal("Shah", result.LastName);
            Assert.Equal("Mr.", result.GenerationOrSuffix);

            EventHubClient.Verify(e => e.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeastOnce);
        }

        private IApplicantService GetApplicantService(List<IApplicant> applicants = null)
        {
            return new ApplicantService
                (
                    new ApplicantRequestValidator(), 
                    new FakeApplicantRepository(new UtcTenantTime(), applicants ?? new List<IApplicant>()),
                    EventHubClient.Object,
                    new UtcTenantTime()
                 );
        }
    }
}