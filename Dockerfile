FROM registry.lendfoundry.com/base:core2 AS build-env
WORKDIR /app

COPY ./src/ /app/
RUN find . -name "global.json"  -type f -delete 
RUN find . -name "NuGet.config"  -type f -delete 
RUN find . -name "*.xproj"  -type f -delete

# Copy csproj and restore as distinct layers
WORKDIR /app/Docitt.Applicant.Api
RUN eval "$CMD_RESTORE"

# Copy everything else and build 
RUN dotnet publish --verbosity normal -c Release -o out --no-restore

# Build runtime image
FROM microsoft/aspnetcore:2.0.6
WORKDIR /app/
COPY --from=build-env /app/Docitt.Applicant.Api/out .

LABEL lendfoundry.branch "LENDFOUNDRY_BRANCH"
LABEL lendfoundry.commit "LENDFOUNDRY_COMMIT"
LABEL lendfoundry.build "LENDFOUNDRY_BUILD_NUMBER"
LABEL lendfoundry.tag "LENDFOUNDRY_TAG"
ENTRYPOINT ["dotnet", "Docitt.Applicant.Api.dll"]