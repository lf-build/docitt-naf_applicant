﻿using LendFoundry.Security.Identity;
using System.Collections.Generic;

namespace Docitt.Applicant
{
    public interface IApplicantService
    {
        IApplicant Add(IApplicantRequest applicant);

        void Update(string applicantId, IApplicant applicant);

        IApplicant Get(string applicantId);

        void Delete(string applicantId);

        IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest);

        void UpdateName(string applicantId, IApplicantNameRequest applicantNameRequest, string applicationNumber);
        

        void LinkToUser(string applicantId, string userId);

        string CreateIdentityToken(string applicantId);

        IApplicant GetByUserId(string userId);

        bool IsTokenValid(string token);

        IApplicant FindByTokenAndLast4Ssn(string token, string lastFourSsn);
    }
}