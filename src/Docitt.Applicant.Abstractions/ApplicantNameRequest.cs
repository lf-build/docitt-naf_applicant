using LendFoundry.Foundation.Persistence;

namespace Docitt.Applicant
{
    public class ApplicantNameRequest : Aggregate, IApplicantNameRequest
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }
       
        public string LastName { get; set; }
       
        public string GenerationOrSuffix { get; set; }
    }
}