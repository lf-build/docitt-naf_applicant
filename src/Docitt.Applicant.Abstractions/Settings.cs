using System;

namespace Docitt.Applicant
{
    public static class Settings
    {       
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "applicant";

    }
}