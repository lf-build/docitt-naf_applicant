﻿using FluentValidation;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;


#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Linq;

namespace Docitt.Applicant.Api.Controllers
{

    /// <summary>
    /// Represent applicant controller class.
    /// </summary>
    [Route("/")]
    public class ApplicantController : ExtendedController
    {
        /// <summary>
        /// Represent constructor class.
        /// </summary>
        /// <param name="applicantService"></param>
        /// <param name="logger"></param>
        public ApplicantController(IApplicantService applicantService, ILogger logger) : base(logger)
        {
            if (applicantService == null)
                throw new ArgumentNullException(nameof(applicantService));

            ApplicantService = applicantService;
        }

        /// <summary>
        /// Applicant service property.
        /// </summary>
        public IApplicantService ApplicantService { get; set; }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// Add applicant
        /// </summary>
        /// <param name="applicant">Applicant's input payload</param>
        /// <returns>IApplicant</returns>
        [HttpPut]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult Add([FromBody]ApplicantRequest applicant)
        {
            try
            {
                return Execute(() => Ok(ApplicantService.Add(applicant)));
            }
            catch (ApplicantWithSameEmailAlreadyExist ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (ValidationException validationException)
            {
                return
                    ErrorResult.BadRequest(
                        validationException.Errors.Select(error => new Error(error.ErrorMessage)));
            }
        }

        /// <summary>
        /// Update applicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="applicant">Applicant's input payload</param>
        /// <returns>NoContentResult</returns>
        [HttpPost("/{applicantId}")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult Update(string applicantId, [FromBody]Applicant applicant)
        {
            try
            {
                return Execute(() =>
                {
                    ApplicantService.Update(applicantId, applicant);
                    return NoContentResult;
                });
            }
            catch (ApplicantWithSameEmailAlreadyExist ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (ValidationException validationException)
            {
                return
                    ErrorResult.BadRequest(
                        validationException.Errors.Select(error => new Error(error.ErrorMessage)));
            }
        }

        /// <summary>
        /// Get applicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>IApplicant</returns>
        [HttpGet("/{applicantId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
       [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult Get(string applicantId)
        {
            return Execute(() => Ok(ApplicantService.Get(applicantId)));
        }

        /// <summary>
        /// Delete applicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>NoContentResult</returns>
        [HttpDelete("/{applicantId}")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult Delete(string applicantId)
        {
            return Execute(() =>
            {
                ApplicantService.Delete(applicantId);
                return NoContentResult;
            });
        }

        /// <summary>
        /// Search applicant
        /// </summary>
        /// <param name="searchApplicantRequest">searchApplicantRequest</param>
        /// <returns>IApplicant[]</returns>
        [HttpPost("/search")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult Search([FromBody]SearchApplicantRequest searchApplicantRequest)
        {
            return Execute(() => Ok(ApplicantService.Search(searchApplicantRequest)));
        }

        /// <summary>
        /// Update Name of applicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="applicantNameRequest">applicantNameRequest</param>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <returns>NoContentResult</returns>
        [HttpPut("/{applicantId}/name/{applicationNumber?}")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult UpdateName(string applicantId, [FromBody]ApplicantNameRequest applicantNameRequest, string applicationNumber)
        {
            return Execute(() =>
            {
                ApplicantService.UpdateName(applicantId, applicantNameRequest, applicationNumber);
                return NoContentResult;
            });
        }

        /// <summary>
        /// LinkToUser
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="userId">userId</param>
        /// <returns>NoContentResult</returns>
        [HttpPut("/{applicantId}/user/{userId}")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult LinkToUser(string applicantId, string userId)
        {
            return Execute(() =>
            {
                ApplicantService.LinkToUser(applicantId, userId);
                return NoContentResult;
            });
        }

        /// <summary>
        /// GetByUserId
        /// </summary>
        /// <param name="userId">userId</param>
        /// <returns>IApplicant</returns>
        [HttpGet("user/{userId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult GetByUserId(string userId)
        {
            return Execute(() => Ok(ApplicantService.GetByUserId(userId)));
        }

        /// <summary>
        /// CreateIdentityToken
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>string</returns>
        [HttpPost("/{id}/create-identity-token")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult CreateIdentityToken(string id)
        {
            return Execute(() => Ok(ApplicantService.CreateIdentityToken(id)));
        }

        /// <summary>
        /// GetByUserIdentityId
        /// </summary>
        /// <param name="identityId">identityId</param>
        /// <returns>IApplicant</returns>
        [HttpGet("/identity/id/{identityId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult GetByUserIdentityId(string identityId)
        {
            return Execute(() => Ok(ApplicantService.GetByUserId(identityId)));
        }

        /// <summary>
        /// VerifyToken
        /// </summary>
        /// <param name="token">token</param>
        /// <returns>bool</returns>
        [HttpPost("/identity/token/{token}/verify")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult VerifyToken(string token)
        {
            return Execute(() =>
            {
                if (ApplicantService.IsTokenValid(token))
                    return NoContentResult;
#if DOTNET2
                return new NotFoundResult();
#else
                    return new HttpNotFoundResult();
#endif
            }
            );
        }

        /// <summary>
        /// FindByTokenAndLastFourSsn
        /// </summary>
        /// <param name="token">token</param>
        /// <param name="last4Ssn">last4Ssn</param>
        /// <returns>IApplicant</returns>
        [HttpGet("/find/token/{token}/last4ssn/{last4Ssn}")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public IActionResult FindByTokenAndLastFourSsn(string token, string last4Ssn)
        {
            return Execute(() => Ok(ApplicantService.FindByTokenAndLast4Ssn(token, last4Ssn)));
        }
    }
}