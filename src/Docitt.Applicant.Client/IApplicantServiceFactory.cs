﻿using Docitt.Applicant;
using LendFoundry.Security.Tokens;

namespace Docitt.Application.Client
{
    public interface IApplicantServiceFactory
    {
        IApplicantService Create(ITokenReader reader);
    }
}
