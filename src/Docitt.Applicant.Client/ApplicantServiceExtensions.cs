﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.Application.Client
{
    public static class ApplicantServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddApplicantService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IApplicantServiceFactory>(p => new ApplicantServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicantServiceFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddApplicantService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IApplicantServiceFactory>(p => new ApplicantServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IApplicantServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicantService(this IServiceCollection services)
        {
            services.AddTransient<IApplicantServiceFactory>(p => new ApplicantServiceFactory(p));
            services.AddTransient(p => p.GetService<IApplicantServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

       
    }
}
