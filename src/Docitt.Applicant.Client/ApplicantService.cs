﻿using Docitt.Applicant;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System;
namespace Docitt.Application.Client
{
    public class ApplicantService : IApplicantService
    {
        public ApplicantService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IApplicant Add(IApplicantRequest applicant)
        {
            var request = new RestRequest("/", Method.PUT);
            request.AddJsonBody(applicant);
            return Client.Execute<Applicant.Applicant>(request);
        }

        public void Update(string applicantId, IApplicant applicant)
        {
            var request = new RestRequest("/{applicantId}", Method.POST);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(applicant);
            Client.Execute(request);
        }

        public IApplicant Get(string applicantId)
        {
            var request = new RestRequest("/{applicantId}", Method.GET);
            request.AddUrlSegment("applicantId", applicantId);
            return Client.Execute<Applicant.Applicant>(request);
        }

        public void Delete(string applicantId)
        {
            var request = new RestRequest("/{applicantId}", Method.DELETE);
            request.AddUrlSegment("applicantId", applicantId);
            Client.Execute(request);
        }

        public IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest)
        {
            var request = new RestRequest("/search", Method.POST);
            request.AddJsonBody(searchApplicantRequest);
            return Client.Execute<List<Applicant.Applicant>>(request);
        }

        public void UpdateName(string applicantId, IApplicantNameRequest applicantNameRequest, string applicationNumber)
        {
            var request = new RestRequest("/{applicantId}/name/{applicationNumber?}", Method.PUT);
            request.AddUrlSegment(nameof(applicantId), applicantId);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);
            request.AddJsonBody(applicantNameRequest);

            Client.Execute(request);
        }
        public IApplicant GetByUserId(string userId)
        {
            var request = new RestRequest("user/{userId}", Method.GET);
            request.AddUrlSegment("userId", userId);
            return Client.Execute<Applicant.Applicant>(request);
        }

        public void LinkToUser(string applicantId, string userId)
        {
            var request = new RestRequest("/{applicantId}/link/{userId}", Method.PUT);
            request.AddUrlSegment(nameof(applicantId), applicantId);
            request.AddUrlSegment(nameof(userId), userId);
            Client.Execute(request);
        }

        public string CreateIdentityToken(string applicantId)
        {
            var request = new RestRequest("/{applicantId}/create-identity-token", Method.POST);
            request.AddUrlSegment(nameof(applicantId), applicantId);
            var response = Client.ExecuteRequest(request);
            Console.WriteLine(response.Content);
            return response.Content;
        }

        public bool IsTokenValid(string token)
        {
            var request = new RestRequest($"/identity/token/{{{nameof(token)}}}/verify", Method.POST);
            request.AddUrlSegment(nameof(token), token);
            return Client.Execute(request);
        }

        public IApplicant FindByTokenAndLast4Ssn(string token, string lastFourSsn)
        {
            var request = new RestRequest("/find/token/{token}/last4ssn/{lastFourSsn}", Method.POST);
            request.AddUrlSegment(nameof(token), token);
            request.AddUrlSegment(nameof(lastFourSsn), lastFourSsn);
            return Client.Execute<Applicant.Applicant>(request);
        }
    }
}