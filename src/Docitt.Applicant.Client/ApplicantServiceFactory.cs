﻿using System;
using Docitt.Applicant;

using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.Application.Client
{
    public class ApplicantServiceFactory : IApplicantServiceFactory
    {

        [Obsolete("Need to use the overloaded with Uri")]
        public ApplicantServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public ApplicantServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; set; }

        private Uri Uri { get; }


        public IApplicantService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("applicant");
            }


            var client = Provider.GetServiceClient(reader, uri);
            return new ApplicantService(client);
        }

    }
}
