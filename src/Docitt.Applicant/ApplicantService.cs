﻿using FluentValidation;
using Docitt.Applicant.Events;

using System;
using System.Collections.Generic;
using LendFoundry.Security.Identity;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.EventHub;

namespace Docitt.Applicant
{
    public class ApplicantService : IApplicantService
    {
        public ApplicantService
        (
            IValidator<IApplicantRequest> applicantRequestValidator,
            IApplicantRepository applicantRepository,
            IEventHubClient eventHub,
            ITenantTime tenantTime
        )
        {

            if (applicantRequestValidator == null)
                throw new ArgumentNullException(nameof(applicantRequestValidator));

            if (applicantRepository == null)
                throw new ArgumentNullException(nameof(applicantRepository));

            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));

            if (tenantTime == null)
                throw new ArgumentNullException(nameof(tenantTime));

            ApplicantRequestValidator = applicantRequestValidator;
            ApplicantRepository = applicantRepository;
            EventHub = eventHub;
            TenantTime = tenantTime;
        }

        private IValidator<IApplicantRequest> ApplicantRequestValidator { get; }

        private IApplicantRepository ApplicantRepository { get; }

        private IEventHubClient EventHub { get; }

        private ITenantTime TenantTime { get; }

        public IApplicant Add(IApplicantRequest applicant)
        {
            var applicantModel = new Applicant(applicant);

            var result = ApplicantRequestValidator.Validate(applicant);
            if (!result.IsValid)
                throw new ValidationException(result.Errors);

            ApplicantRepository.Add(applicantModel);
            return applicantModel;
        }

        public void Update(string applicantId, IApplicant applicant)
        {
            //TODO: Do we need to allow change Applicant's email address?
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            var applicantModel = new Applicant(applicantId, applicant);

            var existingApplicant = Get(applicantId);

            if (!string.Equals(existingApplicant.Email, applicantModel.Email) && ApplicantRepository.GetByEmail(applicantModel.Email) != null)
                throw new ApplicantWithSameEmailAlreadyExist("User with same email address is already exist");

            ApplicantRepository.Update(applicantModel);
        }

        public IApplicant Get(string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            var applicant = ApplicantRepository.Get(applicantId).Result;
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            return applicant;
        }

        public void Delete(string applicantId)
        {
            ApplicantRepository.Remove(Get(applicantId));
        }

        public IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest)
        {
            if (searchApplicantRequest == null)
                throw new ArgumentNullException(nameof(searchApplicantRequest));

            return ApplicantRepository.Search(searchApplicantRequest);
        }

        public void UpdateName(string applicantId, IApplicantNameRequest applicantNameRequest, string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            if (applicantNameRequest == null)
                throw new ArgumentNullException(nameof(applicantNameRequest));

            if (string.IsNullOrWhiteSpace(applicantNameRequest.FirstName))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantNameRequest.FirstName));

            if (string.IsNullOrWhiteSpace(applicantNameRequest.LastName))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantNameRequest.LastName));

            var applicant = ApplicantRepository.Get(applicantId).Result;
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            var eventNameModified = new ApplicantNameModified
            {
                ApplicationNumber = applicationNumber,
                ApplicantId = applicant.Id,
                OldFirstName = applicant.FirstName,
                OldLastName = applicant.LastName,
                OldMiddleName = applicant.MiddleName,
                OldGenerationOrSuffix = applicant.GenerationOrSuffix,
                NewFirstName = applicantNameRequest.FirstName,
                NewLastName = applicantNameRequest.LastName,
                NewMiddleName = applicantNameRequest.MiddleName,
                NewGenerationOrSuffix = applicantNameRequest.GenerationOrSuffix
            };

            ApplicantRepository.UpdateApplicantName(applicantId, applicantNameRequest);

            EventHub.Publish(nameof(ApplicantNameModified), eventNameModified).Wait();
        }
        
        public void LinkToUser(string applicantId, string userId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentException("Argument is null or whitespace", nameof(userId));

            var attachedUser = ApplicantRepository.GetByUserId(userId);
            if (attachedUser != null)
                throw new ArgumentException($"User {userId} is already assigned to another applicant.");

            ApplicantRepository.LinkToUser(applicantId, userId);
        }

        public string CreateIdentityToken(string applicantId)
        {
            var identity = new UserIdentity
            {
                Token = $"{Guid.NewGuid()}",
                TokenExpiration = new TimeBucket(DateTime.UtcNow.AddDays(5)) //TODO Get from configuration
            };

            var applicant = Get(applicantId);

            if (applicant == null || applicant.UserIdentity != null)
                throw new NotFoundException("Applicant not found.");

            applicant.UserIdentity = identity;
            ApplicantRepository.Update(applicant);

            return identity.Token;

        }


        public IApplicant GetByUserId(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new InvalidArgumentException("User ID cannot be empty.");

            var applicant= ApplicantRepository.GetByUserId(userId);

            if (applicant == null)
                throw new NotFoundException("Applicant not found.");

            return applicant;
        }

        public bool IsTokenValid(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("Token cannot be empty.", nameof(token));

            var applicant = ApplicantRepository.GetByToken(token);
            if (applicant?.UserIdentity != null)
            {
                var now = TenantTime.Now;
                var expiration = TenantTime.Convert(applicant.UserIdentity.TokenExpiration.Time);
                Console.WriteLine($"{now} - {expiration}");
                return expiration > now;
            }
            return false;
        }

        public IApplicant FindByTokenAndLast4Ssn(string token, string last4Ssn)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("Argument is null or whitespace", nameof(token));

            if (string.IsNullOrWhiteSpace(last4Ssn))
                throw new ArgumentException("Argument is null or whitespace", nameof(last4Ssn));

            var applicant = ApplicantRepository.GetByToken(token);

            if (applicant == null || !IsTokenValid(token))
                throw new ArgumentException($"Invalid token. Applicant not found or token expired.", nameof(token));

            if(!applicant.Ssn.Substring(applicant.Ssn.Length - 4, 4).Equals(last4Ssn))
                throw new ArgumentException($"Invalid last four ssn digits", nameof(last4Ssn));

            return applicant;
        }
    }
}