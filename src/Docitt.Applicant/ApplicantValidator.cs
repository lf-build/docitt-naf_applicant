﻿using FluentValidation;

namespace Docitt.Applicant
{
    public class ApplicantValidator<T> : AbstractValidator<T> where T: IApplicant
    {
        public ApplicantValidator()
        {
            RuleFor(applicant => applicant.FirstName).NotNull().NotEmpty();
            RuleFor(applicant => applicant.Email)
                .NotNull()
                .NotEmpty()
                .EmailAddress();

            RuleFor(applicant => applicant.Identities)
                .NotNull()
                .NotEmpty();

        }

    }
}
